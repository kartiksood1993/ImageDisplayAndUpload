package com.test.imagetask.services;

import com.test.imagetask.application.ImageTaskApplication;
import com.test.imagetask.utils.NetworkUtility;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
public class BaseService {
    ImageTaskApplication application;
    OkHttpClient.Builder okHttpClientBuilder;

    @Inject
    public BaseService(ImageTaskApplication application) {
        this.application = application;
        createOkHttpClientBuilder();
    }

    private void createOkHttpClientBuilder() {
        okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(URLs.Retrofit.sTime, TimeUnit.SECONDS)
                .readTimeout(URLs.Retrofit.sTime, TimeUnit.SECONDS)
                .writeTimeout(URLs.Retrofit.sTime, TimeUnit.SECONDS)
                .addInterceptor(new NetworkUtility(application.getApplicationContext()));
    }

    public static OkHttpClient.Builder createOkHttpClientBuilder(okhttp3.Cache cache) {
        return new OkHttpClient.Builder().cache(cache);
    }


    public API getClient() {
        String baseUrl = URLs.Retrofit.sAPI_BASE_URL;
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build()
                .create(API.class);
    }
}