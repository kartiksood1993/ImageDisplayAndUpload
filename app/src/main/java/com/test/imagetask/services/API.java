package com.test.imagetask.services;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface API {
    @Multipart
    @POST(URLs.ApiRequest.uploadImage)
    Call<ResponseBody> postImage(@Part MultipartBody.Part image);

    @GET
    Call<Object> getRequest(@Url String url);

}
