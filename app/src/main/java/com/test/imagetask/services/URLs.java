package com.test.imagetask.services;

public class URLs {
   public static class Retrofit {
        public static final String sContentType = "Content-type";
        public static final String sContentTypeJson = "application/json";
        public static final String sAPI_BASE_URL = "https://www.learnovationz.com/machine-task/";
        public static final long sTime = 120;
    }

    public static class ApiRequest {
        public static final String getImageList = "machine-task.php";
        public static final String uploadImage = "upload.php";
    }
}