package com.test.imagetask.application;

import android.app.Application;

import com.test.imagetask.dagger.ApplicationComponent;
import com.test.imagetask.dagger.ApplicationModule;
import com.test.imagetask.dagger.DaggerApplicationComponent;

public class ImageTaskApplication extends Application {
    private static ApplicationComponent applicationComponent;

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static void setApplicationComponent(ApplicationComponent applicationComponent) {
        ImageTaskApplication.applicationComponent = applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setApplicationComponent(DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build());
    }
}
