package com.test.imagetask.modules.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.test.imagetask.R;
import com.test.imagetask.databinding.ImageListItemBinding;
import com.test.imagetask.interfaces.LongItemClickListener;
import com.test.imagetask.modules.dashboard.models.response.ImageDataResponse;

import java.util.ArrayList;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder> {
    ArrayList<ImageDataResponse> imageListResponses;
    Picasso picasso;
    Context context;
    LongItemClickListener longItemClickListener;
    int selectedItemPosition = -1;

    public ImageListAdapter(Context context, ArrayList<ImageDataResponse> imageListResponseArrayList, Picasso picasso, LongItemClickListener longItemClickListener) {
        this.context = context;
        this.imageListResponses = imageListResponseArrayList;
        this.picasso = picasso;
        this.longItemClickListener = longItemClickListener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ImageListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.image_list_item, parent, false);
        return new ImageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        picasso.load(imageListResponses.get(position).getImages()).placeholder(R.drawable.placeholder).resize(400, 400).onlyScaleDown().into(holder.binding.image, new Callback() {
            @Override
            public void onSuccess() {
                holder.binding.progress.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                holder.binding.progress.setVisibility(View.GONE);
            }
        });
        holder.binding.getRoot().setOnLongClickListener(view -> {
            if (selectedItemPosition == position) {
                selectedItemPosition = -1;
            } else {
                selectedItemPosition = position;
            }
            longItemClickListener.selectedItemPosition(selectedItemPosition);
            notifyDataSetChanged();
            return true;
        });
        if (selectedItemPosition == position) {
            holder.binding.container.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        } else {
            holder.binding.container.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
        }
    }

    @Override
    public int getItemCount() {
        return imageListResponses.size();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageListItemBinding binding;

        public ImageViewHolder(ImageListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
