package com.test.imagetask.modules.dashboard.models.response;

import java.io.Serializable;

public class ImageDataResponse implements Serializable {
    private int id;
    private String images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
