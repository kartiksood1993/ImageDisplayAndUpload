package com.test.imagetask.modules.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.Observable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.test.imagetask.R;
import com.test.imagetask.application.ImageTaskApplication;
import com.test.imagetask.databinding.ActivityDashboardBinding;
import com.test.imagetask.interfaces.LongItemClickListener;
import com.test.imagetask.modules.dashboard.adapter.ImageListAdapter;
import com.test.imagetask.modules.dashboard.models.response.ImageDataResponse;
import com.test.imagetask.modules.dashboard.models.response.ImageListResponse;
import com.test.imagetask.utils.Constants;
import com.test.imagetask.utils.ProgressUtility;

import java.util.ArrayList;

import javax.inject.Inject;

public class DashboardActivity extends AppCompatActivity implements LongItemClickListener {

    @Inject
    DashboardViewModel dashboardViewModel;
    @Inject
    Picasso picasso;

    private ImageListAdapter adapter;
    private int selectedItemPosition;
    ArrayList<ImageDataResponse> imageDataResponseArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageTaskApplication.getApplicationComponent().inject(this);
        ActivityDashboardBinding activityDashboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        activityDashboardBinding.setViewmodel(dashboardViewModel);
        ProgressUtility.showProgress(this, "Fetch Images", false);
        dashboardViewModel.getImagesList();
        initRecyclerView(activityDashboardBinding.imageRecyclerView);
        handleImageListResponse();
        handleErrorMessage();
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        adapter = new ImageListAdapter(this, imageDataResponseArrayList, picasso, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this) {
            @Override
            public int scrollVerticallyBy(int dx, RecyclerView.Recycler recycler, RecyclerView.State state) {
                int scrollRange = super.scrollVerticallyBy(dx, recycler, state);
                int overscroll = dx - scrollRange;
                if (overscroll > 0) {

                } else if (overscroll < 0) {
                    // top overscroll
                }
                return scrollRange;
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void handleImageListResponse() {
        dashboardViewModel.imageList.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                ProgressUtility.hideProgress();
                ImageListResponse imageListResponse = dashboardViewModel.imageList.get();
                if (imageListResponse != null) {
                    imageDataResponseArrayList.addAll(imageListResponse.getImageDataResponseArrayList());
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void handleErrorMessage() {
        dashboardViewModel.errorMessage.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                ProgressUtility.hideProgress();
                String message = dashboardViewModel.errorMessage.get();
                if (message != null && !message.isEmpty()) {
                    Toast.makeText(DashboardActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void selectedItemPosition(int position) {
        selectedItemPosition = position;
        dashboardViewModel.showNextButton.set(position != -1);
    }

    public void openDetailScreen(View view) {
        if (selectedItemPosition == -1) {
            return;
        }
        Intent intent = new Intent(this, DashboardDetailScreenActivity.class);
        intent.putExtra(Constants.imageResponse, imageDataResponseArrayList.get(selectedItemPosition));
        startActivity(intent);
    }
}