package com.test.imagetask.modules.dashboard.models.request;

public class SaveImageRequest {
    String image1;

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }
}
