package com.test.imagetask.modules.dashboard;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.Observable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.test.imagetask.R;
import com.test.imagetask.application.ImageTaskApplication;
import com.test.imagetask.databinding.ActivityDashboardDetailScreenBinding;
import com.test.imagetask.modules.dashboard.models.request.CropPhotoRatioModel;
import com.test.imagetask.modules.dashboard.models.response.ImageDataResponse;
import com.test.imagetask.utils.Constants;
import com.test.imagetask.utils.ProgressUtility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

public class DashboardDetailScreenActivity extends AppCompatActivity {

    @Inject
    Picasso picasso;

    @Inject
    DashboardDetailScreenViewModel dashboardDetailScreenViewModel;
    ActivityDashboardDetailScreenBinding activityDashboardDetailScreenBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageTaskApplication.getApplicationComponent().inject(this);
        activityDashboardDetailScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard_detail_screen);
        ImageDataResponse imageDataResponse = (ImageDataResponse) getIntent().getExtras().getSerializable(Constants.imageResponse);
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                try {
                    activityDashboardDetailScreenBinding.crop.setImageBitmap(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.d("exception", e.getMessage());
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d("exception", "");
            }
        };
        picasso.load(imageDataResponse.getImages()).resize(600, 600).onlyScaleDown().into(target);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Crop Screen");
        }
        activityDashboardDetailScreenBinding.setViewmodel(dashboardDetailScreenViewModel);
        handleCropImage();
    }

    public void handleCropImage() {
        dashboardDetailScreenViewModel.cropPhotoRatioModelObservable.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                CropPhotoRatioModel cropPhotoRatioModel = dashboardDetailScreenViewModel.cropPhotoRatioModelObservable.get();
                if (cropPhotoRatioModel != null && isPossibleCrop(cropPhotoRatioModel.getNumberOne(), cropPhotoRatioModel.getNumberTwo())) {
                    activityDashboardDetailScreenBinding.crop.setAspectRatio(cropPhotoRatioModel.getNumberOne(), cropPhotoRatioModel.getNumberTwo());
                } else {
                    Toast.makeText(DashboardDetailScreenActivity.this, "can't crop photo", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dashboardDetailScreenViewModel.saveImage.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                Bitmap bitmap = activityDashboardDetailScreenBinding.crop.getCroppedImage();
                getEncodedImage(bitmap);
            }
        });

        dashboardDetailScreenViewModel.imageResponse.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                ProgressUtility.hideProgress();
                if (dashboardDetailScreenViewModel.imageResponse.get()) {
                    Toast.makeText(DashboardDetailScreenActivity.this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DashboardDetailScreenActivity.this, "Image has not been Uploaded Successfully", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });
    }

    private Boolean isPossibleCrop(int widthRatio, int heightRatio) {
        Bitmap bitmap = activityDashboardDetailScreenBinding.crop.getViewBitmap();
        if (bitmap == null) {
            Toast.makeText(this, "Image not found", Toast.LENGTH_SHORT).show();
            return false;
        }
        return !(bitmap.getWidth() < widthRatio && bitmap.getHeight() > heightRatio);
    }

    private void getEncodedImage(Bitmap bitmap) {
        if (bitmap == null) {
            Toast.makeText(this, "Image not found", Toast.LENGTH_SHORT).show();
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        try {
            File f = new File(getCacheDir(), "image.jpg");
            f.createNewFile();
            //write the bytes in file
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                fos.write(imageBytes);
                fos.flush();
                fos.close();
                dashboardDetailScreenViewModel.saveImage(f);
                ProgressUtility.showProgress(this, "Uploading Image", false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}