package com.test.imagetask.modules.dashboard;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.test.imagetask.modules.dashboard.models.request.CropPhotoRatioModel;
import com.test.imagetask.services.API;
import com.test.imagetask.services.BaseService;
import com.test.imagetask.services.URLs;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardDetailScreenViewModel {
    API api;
    ObservableField<CropPhotoRatioModel> cropPhotoRatioModelObservable = new ObservableField<>();
    ObservableField<Boolean> saveImage = new ObservableField<>();
    ObservableBoolean imageResponse = new ObservableBoolean();

    @Inject
    public DashboardDetailScreenViewModel(BaseService baseService) {
        api = baseService.getClient();
    }


    public void onClickRatio(int number1, int number2) {
        CropPhotoRatioModel cropPhotoRatioModel = new CropPhotoRatioModel();
        cropPhotoRatioModel.setNumberOne(number1);
        cropPhotoRatioModel.setNumberTwo(number2);
        cropPhotoRatioModelObservable.set(cropPhotoRatioModel);
    }


    public void onClickSave() {
        saveImage.set(true);
    }

    public void saveImage(File f) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image1", f.getName(), reqFile);
        api.postImage(body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.body() != null && response.isSuccessful()) {
                    imageResponse.set(true);
                } else {
                    imageResponse.set(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                imageResponse.set(true);
            }
        });
    }
}
