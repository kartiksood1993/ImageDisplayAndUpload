package com.test.imagetask.modules.dashboard.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ImageListResponse {
    @SerializedName("images")
    ArrayList<ImageDataResponse> imageDataResponseArrayList;

    public ArrayList<ImageDataResponse> getImageDataResponseArrayList() {
        if(imageDataResponseArrayList==null){
            return new ArrayList<>();
        }
        return imageDataResponseArrayList;
    }
}
