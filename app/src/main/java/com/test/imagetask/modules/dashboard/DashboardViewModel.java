package com.test.imagetask.modules.dashboard;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.test.imagetask.modules.dashboard.models.response.ImageListResponse;
import com.test.imagetask.services.API;
import com.test.imagetask.services.BaseService;
import com.test.imagetask.services.URLs;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel {
    API api;
    public ObservableField<String> errorMessage = new ObservableField<>();
    public ObservableField<ImageListResponse> imageList = new ObservableField<>();
    public ObservableBoolean showNextButton = new ObservableBoolean();

    @Inject
    public DashboardViewModel(BaseService baseService) {
        api = baseService.getClient();
    }

    public void getImagesList() {
        api.getRequest(URLs.ApiRequest.getImageList).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                Object object = response.body();
                if (object != null) {
                    ImageListResponse imageListResponse = new Gson().fromJson(new Gson().toJson(object), ImageListResponse.class);
                    imageList.set(imageListResponse);
                } else {
                    errorMessage.set("Something went wrong, Please try again later");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                errorMessage.set(t.getMessage());
            }
        });
    }
}
