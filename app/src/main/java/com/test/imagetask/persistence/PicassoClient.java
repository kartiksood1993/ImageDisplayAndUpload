package com.test.imagetask.persistence;

import android.content.Context;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.test.imagetask.services.BaseService;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class PicassoClient {
    Context context;

    public PicassoClient(Context context) {
        this.context = context;
    }

    public Picasso createPicassoClient() {
        Cache cache = new Cache(context.getCacheDir(), (10 * 1024 * 1024));
        OkHttpClient httpClient = BaseService.createOkHttpClientBuilder(cache).build();
        return new Picasso.Builder(context).downloader(new OkHttp3Downloader(httpClient)).build();
    }
}