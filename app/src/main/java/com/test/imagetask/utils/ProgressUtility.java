package com.test.imagetask.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.ProgressBar;

import androidx.core.content.ContextCompat;

import com.test.imagetask.R;


public class ProgressUtility {

    private static ProgressDialog mProgressDialog;

    /*
    To show the progress. This is handled on a single places.
     */
    public static void showProgress(Context context, String pMessage, Boolean pIsCancelable) {
        if (context != null) {
            Activity activity = (Activity) context;
            if (activity.isFinishing())
                return;
            /*
             * If the progress bar is already showing need not create a new one
             */
            if (mProgressDialog == null || !mProgressDialog.isShowing()) {
                mProgressDialog = ProgressDialog.show(context, null, null);

                /*
                 *  To show progress bar with and without message
                 */
                if ((pMessage == null) || (pMessage.trim().
                        equals(Constants.sEmptyString))) {
                    ProgressBar spinner = new android.widget.ProgressBar(context,
                            null, android.R.attr.progressBarStyle);
                    spinner.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor
                                    (context, R.color.colorPrimary),
                            android.graphics.PorterDuff.Mode.MULTIPLY);
                    if (mProgressDialog.getWindow() != null) {
                        mProgressDialog.getWindow().setBackgroundDrawable
                                (new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    }
                    mProgressDialog.setContentView(spinner);
                } else {
                    mProgressDialog.setMessage(pMessage);
                }
                mProgressDialog.setCancelable(pIsCancelable);
                mProgressDialog.show();
            }
        }
    }

    // hide progress
    public static void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

}
