package com.test.imagetask.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.test.imagetask.R;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class NetworkUtility implements Interceptor {
    private Context context;

    public NetworkUtility(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (!isInternetAvailable()) {
            throw new NoInternetException(
                    context.getString(R.string.check_internet_connection));
        }
        try {
            return chain.proceed(chain.request());
        } catch (Exception exception) {
            throw new NoServerFoundException(exception.getMessage());
        }
    }

    Boolean isInternetAvailable() {
        boolean result = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    result = true;
                } else if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    result = true;
                }
            }
        }
        return result;
    }

    public static class NoInternetException extends IOException {
        String message;

        NoInternetException(String message) {
            this.message = message;
        }

        @Nullable
        @Override
        public String getMessage() {
            return message;
        }
    }

    public static class NoServerFoundException extends IOException {
        String message;

        public NoServerFoundException(String message) {
            this.message = message;
        }

        @Nullable
        @Override
        public String getMessage() {
            return message;
        }
    }
}
