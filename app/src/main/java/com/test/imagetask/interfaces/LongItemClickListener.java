package com.test.imagetask.interfaces;

public interface LongItemClickListener {
    public void selectedItemPosition(int position);
}
