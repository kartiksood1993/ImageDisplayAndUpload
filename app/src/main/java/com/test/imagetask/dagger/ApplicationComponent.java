package com.test.imagetask.dagger;

import com.test.imagetask.application.ImageTaskApplication;
import com.test.imagetask.modules.dashboard.DashboardActivity;
import com.test.imagetask.modules.dashboard.DashboardDetailScreenActivity;
import com.test.imagetask.services.BaseService;

import dagger.Component;

@Component(modules = {ApplicationModule.class})
@ApplicationScope
public interface ApplicationComponent {
    ImageTaskApplication getApplicationInstance();

    BaseService provideBaseService();

    void inject(DashboardActivity dashboardActivity);
    void inject(DashboardDetailScreenActivity dashboardDetailScreenActivity);
}