package com.test.imagetask.dagger;

import com.squareup.picasso.Picasso;
import com.test.imagetask.application.ImageTaskApplication;
import com.test.imagetask.persistence.PicassoClient;
import com.test.imagetask.services.BaseService;

import dagger.Module;
import dagger.Provides;

@Module()
public class ApplicationModule {
    private ImageTaskApplication application;

    public ApplicationModule(ImageTaskApplication application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    ImageTaskApplication providesApplication() {
        return application;
    }

    @Provides
    @ApplicationScope
    BaseService provideBaseService() {
        return new BaseService(application);
    }

    @Provides
    @ApplicationScope
    Picasso providePicasso() {
        return new PicassoClient(application).createPicassoClient();
    }
}